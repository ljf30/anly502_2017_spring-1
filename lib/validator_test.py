#!/usr/bin/env python
#
# validate and build a student submission
# Must run in Python 3.4+ because Python 3 is the future. (Unicode, etc.)


import py.test
from validator import *
def test_validate_txt():
    assert validate_txtfile("test_txtfile.txt")==True
    assert validate_txtfile("test_docfile.doc")==False
    assert validate_txtfile("test_docxfile.docx")==False
    assert validate_txtfile("test_rtffile.rtf")==False

def test_ignore_filename():
    assert ignore_filename("test.py")==False
    assert ignore_filename("test.py~")==True
    assert ignore_filename("._test.py")==True
    assert ignore_filename("mydir/")==True

def test_validate_pyfile():
    import tempfile
    assert validate_pyfile("no_file_exists.txt")==False
    f = tempfile.NamedTemporaryFile(mode="w+",suffix=".bad")
    assert validate_pyfile(f.name)==False
    f = tempfile.NamedTemporaryFile(mode="w+",suffix=".py")
    f.write("print(3);\n")
    f.flush()
    assert validate_pyfile(f.name)==True
    f = tempfile.NamedTemporaryFile(mode="w+",suffix=".py",delete=False)
    f.write("a = 3 +;\n")
    f.flush()
    assert validate_pyfile(f.name)==False

def test_Validator():
    v = Validator(cfg_fname="validator_cfg.dist",user_fname="user_cfg.dist")
    assert v.name == "DEMO"
    assert "test_txtfile.txt" in v.required
    assert "optional_present.txt" in v.optional
    assert v.check_files()==0
    assert v.dirname()=="pat-DEMO"
    #
    # try making a zip file
    zfilename = v.build_zip()
    assert os.path.exists(zfilename)
    assert zfilename.endswith(".zip")

    #
    # now verify that it works
    assert v.validate_zip(zfilename)==0
    #
    # Now remove what we unpacked
    os.unlink("unpack/__pycache__/demo_pyfile.cpython-34.pyc")
    os.unlink("unpack/demo_pyfile.py")
    os.unlink("unpack/optional_present.txt")
    os.unlink("unpack/test_txtfile.txt")
    os.rmdir("unpack/__pycache__")
    os.rmdir("unpack")
    os.unlink("pat-DEMO.zip")

if __name__=="__main__":
    test_Validator()

