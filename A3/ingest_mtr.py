#!/usr/bin/env python35
#
# Ingest the TXT file generated by "mtr" and output single line for each file
#
import sys
if sys.version < "3.4":
    raise RuntimeError("validator requires Pyton 3.4 or above")

import datetime,re

def process_mtr_txt(input,output):
    """Reads a mtr_txt input and outputs single records"""
    last_step = 0
    steps = []
    split_re = re.compile("([^0-9() ][^() ]+)? ?\(?([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)")
    for line in input:
        if not line.startswith("MTR.0.85;"):
            raise RuntimeError("Unknown line in input: "+line)
        (ver,t,ok,dest,step,hostip,usec) = line.strip().split(";")
        t = datetime.datetime.fromtimestamp(float(t))

        # If we reached bad data (???) or this is the start of the
        # next MTR output, print the buffer and clear it.
        if step=="1" or hostip=="???":
            # Beginning of new block; create a data record
            if steps:
                output.write(",".join(steps))
                output.write("\n")
            last_step = 0
            steps = []

        # Check to make sure that this is the next step in sequence
        if int(step) != last_step + 1:
            # We have a break. Zero out the steps...
            steps = None
            
        # If steps are good, record this step and keep going
        if steps != None:
            m = split_re.search(hostip)
            if m:
                steps += [step,
                          m.group(1) if m.group(1) else "",
                          m.group(2) if m.group(2) else "",
                          usec]
                last_step = int(step)
            else:
                raise RuntimeError("cannot parse hostname: "+hostip)
    if steps != None:
        output.write(",".join(steps))
        output.write("\n")
        

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("infile",nargs="*",help="input file",default="mtr.www.comcast.com.2016.subset.txt")
    args = parser.parse_args()

    process_mtr_txt(open(args.infile[0],"rU"), sys.stdout)

            
