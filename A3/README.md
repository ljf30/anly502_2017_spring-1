# ANLY 502 Assignment 3

***DRAFT: NOT YET RELEASED***

_Note: Read this assignment online at [https://bitbucket.org/ANLY502/anly502_2017_spring/src/HEAD/A3/?at=master](https://bitbucket.org/ANLY502/anly502_2017_spring/src/HEAD/A3/?at=master)_

In this problem set, you will analyze a network flow dataset that was created for this course. 

This dataset is based on the [`mtr`](https://en.wikipedia.org/wiki/MTR_(software)) command, a popular open source _traceroute_ command that has an interactive, character-based display. The program performs multiple traceroute operations over time and displays the results.  One of the common uses of the `mtr` command is to diagnose network problems.  Take a moment now and [review the Wikipedia page for the mtr command](https://en.wikipedia.org/wiki/MTR_(software)).

The `mtr` command has an option to produce output as a text file.  here is an example of the output:

    MTR.0.85;1482973562;OK;www.comcast.com;1;192.168.10.1;1669
    MTR.0.85;1482973562;OK;www.comcast.com;2;96.120.104.177;16404
    MTR.0.85;1482973562;OK;www.comcast.com;3;68.87.130.233;11504
    MTR.0.85;1482973562;OK;www.comcast.com;4;ae-53-0-ar01.capitolhghts.md.bad.comcast.net (68.86.204.217);14874
    MTR.0.85;1482973562;OK;www.comcast.com;5;be-33657-cr02.ashburn.va.ibone.comcast.net (68.86.90.57);13030
    MTR.0.85;1482973562;OK;www.comcast.com;6;be-10102-cr01.newark.nj.ibone.comcast.net (68.86.85.162);19641
    MTR.0.85;1482973562;OK;www.comcast.com;7;be-10203-cr02.newyork.ny.ibone.comcast.net (68.86.85.186);18920
    MTR.0.85;1482973562;OK;www.comcast.com;8;be-10305-cr02.350ecermak.il.ibone.comcast.net (68.86.85.202);40477
    MTR.0.85;1482973562;OK;www.comcast.com;9;be-7922-ar02-d.northlake.il.ndcchgo.comcast.net (68.86.87.70);40555
    MTR.0.85;1482973562;OK;www.comcast.com;10;69.139.178.142;59072
    MTR.0.85;1482973562;OK;www.comcast.com;11;???;0

In this example, `mtr` has been used from a home to the host `www.comcast.com`. The fields are separated by semicolons. The fields are:

1. Version number, in this case `MTR.0.85`
2. The Unix timestamp of when the `mtr` command was started.
3. An `OK` indicating that the command terminated successfully.
4. The number of hops out that the ICMP PING packet traveled before it returned.
5. The IP address that the packet reached, and optionally the IP address.
6. The number of microseconds that the packet required for the round-trip.

From July 1, 2016 through Dec 28, 2016, we ran `mtr` every minute, generating _traceroute_ results from a [Raspberry PI](https://en.wikipedia.org/wiki/Raspberry_Pi) computer to the host `www.comcast.com`. The Raspberry PI is connected by Ethernet to a cable Apple Airport Extreme Router, that is connected to a Comcast cable modem, all of the variances between subsequent runs are the result of congestion and other network issues within the Comcast network.  

In this assignment you will develop software to help analyze this dataset. A truism of this data science is that it is always easier to collect data than to analyze it. For example, if you scour the Internet you will find many resources for _generating_ traceroute data, but you will find partically no resources for _analyzing_ traceroute data.

Wet set up this $35 computer to collect data because we observed that the Comcast performance was inconsistent. But why was it inconsistent? Network congestion? Is it worse on different days, or at different times of the day? Is the problem congestion, or is it reliability---are there links that are going down?  In this problem set, you'll try to figure that all out.


## Understanding the dataset

The Raspberry PI was configured to run a traceroute every minute from the home network to `www.comcast.com`. The rational of choosing `www.comcast.com` is that the host was likely to be inside the Comcast network, so we weren't looking at problems that might originate anywhere else. We have uploaded the dataset to [s3://gu-anly502/A3/mtr.www.comcast.com.2016.txt](s3://gu-anly502/A3/mtr.www.comcast.com.2016.txt). The 206MB dataset has 2,397,086 lines and a total of 260,499 separate transactions. In other words, this dataset is not very largely, and you could easily analyze it on your laptop.

So here are some questions that we will try to answer:

* How consistent is the Comcast network? 
* Are there network outages? Can we distinguish them from outages at the end-user location?
* How often are there changes within the Comcast network? Are these changes in response to network problems?
* How much redundancy is there within the Comcast network?
* We are getting the service that we are paying for?

We won't be able to answer all of these questions in **A3**, but you might think about answering them in a final project. Also, note that that analyzing this dataset is a *passive analysis* project, but for your final project you could carry out your own measurements.

## Getting Started

As collected, the dataset is hard to analyze, because each time we invoke the `mtr` command the result may be one or more lines.  So the first thing to do is to turn the multi-line format into single records that are more easily handled by conventional data processing  techniques.

To help you get started, we have created a file [`mtr.www.comcast.com.2016.subset.txt`](mtr.www.comcast.com.2016.subset.txt) that contains a subset of the dataset, and a program called [`ingest_mtr.py`](ingest_mtr.py) that reads this file and outputs complete records. The record consists of one or more quads, all separated by commas, where each quad consists of:

* Step number (1..N)
* Hostname of the remote system (or null)
* IP address of the remote system
* The number of microseconds for the response

The splitting of the name returned by `mtr` into a hostname and IP address is done with a regular expression. You should examine the source code of the `ingest_mtr.py` program to see how this is done.

The first record looks like this:

    1,,192.168.10.1,1798,2,,96.120.104.177,9739,3,,68.87.130.233,11766,4,ae-53-0-ar01.capitolhghts.md.bad.comcast.net,68.86.204.217,11203,5,be-33657-cr02.ashburn.va.ibone.comcast.net,68.86.90.57,14575,6,he-0-2-0-0-ar01-d.westchester.pa.bo.comcast.net,68.86.94.226,17923,7,bu-101-ur21-d.westchester.pa.bo.comcast.net,68.85.137.213,16070,8,,68.87.29.59,16761

Even with our preprocessing script, this is a challenging assignment because every input line has a (potentially) different length. So with 2.4 million data records in the original dataset, this might blow up real fast...

The dataset represents a series of paths through the Internet at different times. A (fictional) single line that looks like this:


    1,,192.168.10.1,1000,2,,68.10.20.1,1200,3,,70.20.40.1,2000
    
This line represents two hops:

    192.168.10.1 -> 68.10.20.1      (200 usec)
    68.10.20.1 -> 70.20.40.1        (800 usec)
    
Schematically, we can call this:

    A->B->C

Where A is `192.168.10.1`, B is `68.10.20.1` and C is `70.20.40.1`. 
        
Sometimes you might want to look for a link that goes down and comes back up. Of course, if a link goes down, the _traceroute_ done by `mtr` will stop. So you might end up seeing data that looks like this:

    1: A->B->C->D->E
    2: A->B->C->D->E
    3: A->B->C->D->E
    4: A->B->C->D->E
    5: A->B->C
    6: A->B->C
    7: A->B->C
    8: A->B->C->D->E
    9: A->B->C->D->E

In this example, the link between C and D went down at time 5 and came back up at time 8. The link between D and E may have been up at times 5, 6 and 7, but it may not have been. We have no way of telling for sure.

Note: there are some IP addresses in this dataset that have more than one hostname!

## Question 1: Rough Charcterization

In Question 1 we are asking that you characterize the data. You should answer all of these questions by using `ingest_mtr.py` to process the dataset into single records and then storing the results either in S3 or HDFS. You should then write a map/reduce job that answers the questions, and stores the requested results in the named files.


1. Create a mapper and reducer with  [MRJOB](https://github.com/Yelp/mrjob) called `q1_ipaddresses.py` that computes how many different IP addresses are in the dataset.  Store the output of your program in a file called `q1_ipaddresses.txt`. Each line of the file should have the format:

    _ipaddress_ TAB _count_ 
    
    The file does not need to be sorted.

2. Create a mapper and reducer with MRJOB called `q1_hostnames.py` that computes how many different hostnames are in the dataset.  Store the output of your program in a file called `q1_hostnames.txt`. Each line of the file should have the format:

    _hostname_ TAB _count_
    
    The file does not need to be sorted.

## Question 2: Link Analysis

For this question, we define a link as the transition from one host to another host. You can compute the time associated with the first host from the time associated with the second host. So if the highest step number on a line is 5, that line describes 4 links.

1. Create a link analysis program with MRJOB named `q2_mrjob.py` that will analyze the dataset for all of the links. Create a file `q2_links.txt` which contains, for each link, a line in the following format:

     IPADDRESS1 -> IPADDRESS2 TAB COUNT
     
     The file does not need to be sorted.

2. Modify `q2_mrjob.py` so that it calculates the standard deviation for each link. Generate an output file called `q2_linksdev.txt` that has this format:

     IPADDRESS1 -> IPADDRESS2 TAB COUNT  TAB STDDEV
     
     The file does not need to be sorted.
     
Turn in `q2_mrjob.py` and `q2_linksdev.txt`.      
     
## Question 3: Time Analysis      

Finally, let's look at how Comcast's residential distribution network might be impacted by people watching videos in the evenings. That is, we're going to look for the "Netflix" effect. To do this, we will restrict our analysis of the standard deviation of the link from the 2nd hop to the 3rd hop, and see how it changes for different times of the day.  

1. Create a new program using MRJOB called `q3_hop23.py` that computes the standard deviation between the 2nd and 3rd hop of the dataset by time of day. Generate an output file `q3_hop23.txt` that has this form:

     HOUR TAB STDDEV
     
## Extra Credit

1. For extra credit, create a program called `q3_garpher.py` that makes a graph of this dataset with matplotlib, and turn in the graph called `q2_graph.png`.

## Other ideas

Here are some other things that you could do with the dataset:

* Figure out which links are the least reliable. 
* When does Comcast change the topology of the network? Is it correlated with network outages?
* When are the names associated with IP addresses changed? Does it correlate with other network changes?

Is there a better way to do this analysis?  There are graph-oriented databases. Are they better for doing this kind of analysis?


